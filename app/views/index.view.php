<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <h4 class="pt-4">The overall amount of taxes collected per state</h4>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">State</th>
                    <th scope="col">Total amount of taxes</th>
                    <th scope="col">Country</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($states as $state): ?>
                <tr>
                    <td><?=$state->state_name ?></td>
                    <td><?=$state->total_amount ?></td>
                    <td><?=$state->country_name ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <h4>The average amount of taxes collected per state</h4>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">State</th>
                    <th scope="col">Average amount of taxes</th>
                    <th scope="col">Country</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($states as $state): ?>
                <tr>
                    <td><?=$state->state_name ?></td>
                    <td><?=$state->average_amount ?></td>
                    <td><?=$state->country_name ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <h4>The the average county tax rate per state</h4>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">State</th>
                    <th scope="col">Average county tax rate</th>
                    <th scope="col">Country</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($states as $state): ?>
                <tr>
                    <td><?=$state->state_name ?></td>
                    <td><?=$state->average_rate ?></td>
                    <td><?=$state->country_name ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <h4>The average tax rate of the country </h4>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Country</th>
                    <th scope="col">Average county tax rate</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($countries as $country): ?>
                <tr>
                <td><?=$country->country_name ?></td>
                    <td><?=$country->average_rate ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <h4>The collected overall taxes of the country</h4>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Country</th>
                    <th scope="col">Total amount of taxes</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($countries as $country): ?>
                <tr>
                <td><?=$country->country_name ?></td>
                    <td><?=$country->total_amount ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>