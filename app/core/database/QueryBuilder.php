<?php

namespace App\Core\Database;

use App\Models\{County, State, Country};
use PDO;
use PDOException;

class QueryBuilder
{
    protected $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function prepareTables()
    {
        try {
            $result = $this->pdo->prepare(
                'create table if not exists countries (
                    id int primary key auto_increment,
                    name varchar(255) not null
                )'
            );
            $result->execute();

            $result = $this->pdo->prepare(
                'create table if not exists states (
                    id int primary key auto_increment,
                    name varchar(255) not null,
                    country_id int not null,
                    foreign key (country_id) 
                        references countries(id)
                        on delete cascade
                )'
            );
            $result->execute();

            $result = $this->pdo->prepare(
                'create table if not exists counties (
                    id int primary key auto_increment,
                    name varchar(255) not null,
                    state_id int not null,
                    tax_rate int not null,
                    tax_amount int not null,
                    foreign key (state_id) 
                        references states(id)
                        on delete cascade
                )'
            );
            $result->execute();
        
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function prepareDummyData()
    {
        $result = $this->pdo->prepare('select * from countries');
        $result->execute();
        if(!$result->fetch(PDO::FETCH_ASSOC)) {
            try {
                $this->pdo->exec(
                    "INSERT INTO countries (name) VALUES ('Germany'), ('Belgium'), ('Poland')"
                );
            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }

        $result = $this->pdo->prepare('select * from states');
        $result->execute();
        if(!$result->fetch(PDO::FETCH_ASSOC)) {
            try {
                $this->pdo->exec(
                    "INSERT INTO states (name, country_id) VALUES ('West', 1), ('East', 2), ('North', 1), ('South', 1), ('Center', 1)"
                );
            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }

        $result = $this->pdo->prepare('select * from counties');
        $result->execute();
        if(!$result->fetch(PDO::FETCH_ASSOC)) {
            try {
                $this->pdo->exec(
                    "INSERT INTO counties (name, state_id, tax_rate, tax_amount) VALUES ('First', 1, 4, 5000), 
                    ('Second', 2, 2, 7000),
                    ('Third', 1, 6, 3000),
                    ('Fourth', 2, 3, 9000),
                    ('Fifth', 3, 7, 2500),
                    ('Sixth', 1, 8, 6000),
                    ('Seventh', 1, 5, 3000)"
                );
            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }
    }

    public function selectLatest($table, $class)
    {
        $statement = $this->pdo->prepare("select * from {$table} ORDER BY id DESC LIMIT 1");

        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS, "App\\Models\\$class")[0];
    }
    
    public function selectAll($table)
    {
        $statement = $this->pdo->prepare("select * from {$table}");

        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS);
    }

    public function insert($table, $parameters)
    {
        $sql = sprintf(
            'INSERT INTO %s (%s) VALUES (%s)',
            $table,
            implode(', ', array_keys($parameters)),
            "'".implode("', '", array_values($parameters))."'"
        );

        try {
            $statement = $this->pdo->prepare($sql);

            // die(var_dump($statement));

            $statement->execute($parameters);

        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function fetchStates()
    {
        return $this->pdo->query('
            select 
                countries.name as country_name,
                states.name as state_name, 
                ifnull(sum(tax_amount), 0) as total_amount,
                ifnull(avg(tax_amount), 0) as average_amount,
                ifnull(avg(tax_rate), 0) as average_rate
            from states
            left join counties
                on states.id = counties.state_id
            inner join countries
                on countries.id = states.country_id
            group by states.id
        ')
        ->fetchAll(PDO::FETCH_OBJ);
    }

    function fetchCountries()
    {
        return $this->pdo->query('
            select 
                countries.name as country_name, 
                ifnull(avg(counties.tax_rate), 0) as average_rate,
                ifnull(sum(counties.tax_amount), 0) as total_amount
            from countries
            left join states
                on countries.id = states.country_id
            left join counties
                on states.id = counties.state_id
            group by countries.name
        ')
        ->fetchAll(PDO::FETCH_OBJ);
    }

    public function truncate() {
        try {
            County::truncate();
            State::truncate();
            Country::truncate();
        } catch(PDOException $e) {
            die($e->getMessage());
        }
    }
}