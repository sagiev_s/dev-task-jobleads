<?php

use App\Core\App;

App::get('database')->prepareTables();
App::get('database')->prepareDummyData();
