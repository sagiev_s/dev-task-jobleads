<?php

namespace App\Models;

use App\Core\App;

class State {

    public static function all() {
        return App::get('pdo')->query('
            select 
                countries.name as country_name,
                states.name as state_name, 
                ifnull(sum(tax_amount), 0) as total_amount,
                ifnull(avg(tax_amount), 0) as average_amount,
                ifnull(avg(tax_rate), 0) as average_rate
            from states
            left join counties
                on states.id = counties.state_id
            inner join countries
                on countries.id = states.country_id
            group by states.id
        ')
            ->fetchAll(\PDO::FETCH_OBJ);
    }
    
    public function overallAmount() {
        return App::get('pdo')->query('
            select 
                sum(tax_amount) overall_amount
            from
                counties
            where 
                state_id = '.$this->id.'
        ')
            ->fetch()['overall_amount'];
    }

    public function averageAmount() {
        return App::get('pdo')->query('
            select 
                avg(tax_amount) average_amount
            from
                counties
            where 
                state_id = '.$this->id.'
        ')
            ->fetch()['average_amount'];
    }

    public function averageRate() {
        return App::get('pdo')->query('
            select 
                avg(tax_rate) average_rate
            from
                counties
            where 
                state_id = '.$this->id.'
        ')
            ->fetch()['average_rate'];
    }

    public static function truncate() {
        App::get('pdo')->prepare(
            "DROP TABLE IF EXISTS states"
        )->execute();
    }
}