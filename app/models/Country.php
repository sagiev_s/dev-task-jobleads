<?php

namespace App\Models;

use App\Core\App;

class Country {
    public static function all() {
        return App::get('pdo')->query('
            select 
                countries.name as country_name, 
                ifnull(avg(counties.tax_rate), 0) as average_rate,
                ifnull(sum(counties.tax_amount), 0) as total_amount
            from countries
            left join states
                on countries.id = states.country_id
            left join counties
                on states.id = counties.state_id
            group by countries.name
        ')
            ->fetchAll(\PDO::FETCH_OBJ);
    }

    public function averageRate() {
        return App::get('pdo')->query('
            select avg(tax_rate) as average_rate
            from countries
            inner join states
            on states.country_id = countries.id
            inner join counties
            on counties.state_id = states.id
            where countries.id = '.$this->id.'
        ')
            ->fetch()['average_rate'];
    }

    public function overallAmount() {
        return App::get('pdo')->query('
            select sum(tax_amount) as overall_amount
            from countries
            inner join states
            on states.country_id = countries.id
            inner join counties
            on counties.state_id = states.id
            where countries.id = '.$this->id.'
        ')
            ->fetch()['overall_amount'];
    }

    public static function truncate() {
        App::get('pdo')->prepare(
            "DROP TABLE IF EXISTS countries"
        )->execute();
    }
}