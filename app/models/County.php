<?php

namespace App\Models;

use App\Core\App;

class County {
    public static function truncate() {
        App::get('pdo')->prepare(
            "DROP TABLE IF EXISTS counties"
        )->execute();
    }
}