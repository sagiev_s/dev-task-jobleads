# Jobleads development task

Demo:
[http://ecoworldkz.myjino.ru](http://ecoworldkz.myjino.ru)

## Getting started

First, clone the repository and cd into it:

```bash
git clone https://sagiev_s@bitbucket.org/sagiev_s/dev-task-jobleads.git
cd dev-task-jobleads
```

Run composer:

```bash
composer install
```

Set config on config.php:
```bash
'database' => [
        'name' => 'jobleads1',
        'username' => 'root',
        'password' => '',
        'connection' => 'mysql:host=127.0.0.1',
        'options' => []
    ]
```

Also you can check tests of how correcly it calculating statistics by running PHPUnit:
```bash
vendor/bin/phpunit
```

Run the project:

```bash
php -S localhost:8000
```