<?php

class StringToul
{
    /**
     * Concatenate multiple strings.
     *
     * @param array $strings
     * @return string
     */
    public static function concat( ...$strings) : string {
        $result = "";
        foreach ($strings as $string) {
            $result .= $string;
        }
        return trim($result);
    }


    /**
     * Write string in new line.
     *
     * @param  string  $a
     * @return void
     */
    public static function writeLn(string $a) {
        echo $a . "\n";
    }

    /**
     * Convert the given string to upper-case.
     *
     * @param  string  $string
     * @return string
     */
    public static function upperCase(string $string) : string {
        return strtoupper($string);
    }

    /**
     * Convert the given string to lower-case.
     *
     * @param  string  $string
     * @return string
     */
    public static function lowerCase(string $string) : string {
        return strtolower($string);
    }

    /**
     * Hash the given string with md5 function.
     *
     * @param  string  $string
     * @return string
     */
    public static function hash(string $string) : string {
        return self::md5($string);
    }

    /**
     * Hash the given string with md5 function.
     *
     * @param  string  $string
     * @return string
     */
    public static function md5(string $string) : string {
        return md5($string);
    }

    /**
     * Hash the given string with sha512.
     *
     * @param  string  $string
     * @return string
     */
    public static function sha512(string $string) : string {
        return sha($string);
    }

    /**
     * Replace all occurrences of the search string with the replacement string
     *
     * @param string $search - Needle to search for
     * @param string $replace Replace - value to replace the needle
     * @param string $subject Haystack - string to search the needle in
     * @return string
     */
    public static function replace(string $search, string $replace, string $subject) : string {
        return str_replace($search, $replace, $subject);
    }


    /**
     * Strip whitespace (or other characters) from the beginning and end of a string
     *
     * @param $string
     * @return string
     */
    public static function trim(string $string) : string {
        return trim($string);
    }
}

