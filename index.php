<?php

require 'app/core/bootstrap.php';

require 'app/core/database/migrate.php';

use App\Models\{State, Country};

$states = State::all();

$countries = Country::all();

require 'app/views/index.view.php';