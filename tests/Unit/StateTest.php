<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class StateTest extends TestCase
{
    use TestHelper;

    /** @test */
	public function output_the_overall_amount_of_taxes_collected_per_state() {
        $state = $this->create('states', 'State');
        $firstCountyOfState = $this->create('counties', 'County', ['state_id' => $state->id, 'tax_amount' => 300]);
        $secondCountyOfState = $this->create('counties', 'County', ['state_id' => $state->id, 'tax_amount' => 200]);
        $countyNotOfState = $this->create('counties', 'County', ['tax_amount' => 500]);
        $expectedOverallAmount = 500;
        $this->assertEquals($expectedOverallAmount, $state->overallAmount());
    }
    
    /** @test */
	public function output_the_average_amount_of_taxes_collected_per_state() {
        $state = $this->create('states', 'State');
        $firstCountyOfState = $this->create('counties', 'County', ['state_id' => $state->id, 'tax_amount' => 300]);
        $secondCountyOfState = $this->create('counties', 'County', ['state_id' => $state->id, 'tax_amount' => 200]);
        $countyNotOfState = $this->create('counties', 'County', ['tax_amount' => 500]);
        $expectedAverageAmount = 250;
        $this->assertEquals($expectedAverageAmount, $state->averageAmount());
    }
    
    /** @test */
	public function output_the_average_county_tax_rate_per_state() {
        $state = $this->create('states', 'State');
        $firstCountyOfState = $this->create('counties', 'County', ['state_id' => $state->id, 'tax_rate' => 4]);
        $secondCountyOfState = $this->create('counties', 'County', ['state_id' => $state->id, 'tax_rate' => 2]);
        $countyNotOfState = $this->create('counties', 'County', ['tax_rate' => 7]);
        $expectedAverageRate = 3;
        $this->assertEquals($expectedAverageRate, $state->averageRate());
	}
}