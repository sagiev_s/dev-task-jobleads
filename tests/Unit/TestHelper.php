<?php

namespace Tests\Unit;

use App\Core\App;

trait TestHelper {
    public function create($table, $class, $parameters = null) {
        $database = App::get('database');
        switch($table) {
            case 'countries':
                $database->insert('countries', [
                    'name' => 'Dummy country'
                ]);
                break;
            case 'states':
                $database->insert('states', [
                    'name' => 'Dummy state',
                    'country_id' => $parameters ? $parameters['country_id'] : $this->create('countries', 'Country')->id, 
                ]);
                break;
            case 'counties':
                if(!$parameters) {
                    $database->insert('states', [
                        'name' => 'Dummy country'
                    ]);
                }
                $database->insert('counties', [
                    'name' => 'Dummy county',
                    'state_id' => $parameters && array_key_exists('state_id', $parameters) ? $parameters['state_id'] : $this->create('states', 'State')->id,
                    'tax_rate' => $parameters && array_key_exists('tax_rate', $parameters) ? $parameters['tax_rate'] : 1,
                    'tax_amount' => $parameters && array_key_exists('tax_amount', $parameters) ? $parameters['tax_amount'] : 1,
                ]);
                break;
        }
        return $database->selectLatest($table, $class);
    }

    protected function setUp() : void {
        App::get('database')->prepareTables();
    }

    protected function tearDown() : void {
        App::get('database')->truncate();
        App::get('database')->prepareTables();
    }
}