<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class CountryTest extends TestCase
{
    use TestHelper;
    
    /** @test */
	public function output_the_average_tax_rate_of_the_country() {
        $country = $this->create('countries', 'Country');

        $firstStateOfCountry = $this->create('states', 'State', ['country_id' => $country->id]);
        $firstCountyOfFirstState = $this->create('counties', 'County', ['state_id' => $firstStateOfCountry->id, 'tax_rate' => 5]);
        $secondCountyOfFirstState = $this->create('counties', 'County', ['state_id' => $firstStateOfCountry->id, 'tax_rate' => 2]); // 16
        $thirdCountyOfFirstState = $this->create('counties', 'County', ['state_id' => $firstStateOfCountry->id, 'tax_rate' => 9]);

        $secondStateOfCountry = $this->create('states', 'State', ['country_id' => $country->id]);
        $firstCountyOfSecondState = $this->create('counties', 'County', ['state_id' => $secondStateOfCountry->id, 'tax_rate' => 2]); // 8
        $secondCountyOfSecondState = $this->create('counties', 'County', ['state_id' => $secondStateOfCountry->id, 'tax_rate' => 6]);

        $thirdStateOfCountry = $this->create('states', 'State', ['country_id' => $country->id]);
        $firstCountyOfSecondState = $this->create('counties', 'County', ['state_id' => $thirdStateOfCountry->id, 'tax_rate' => 8]); // 11
        $secondCountyOfSecondState = $this->create('counties', 'County', ['state_id' => $thirdStateOfCountry->id, 'tax_rate' => 3]);

        $fourthStateOfCountry = $this->create('states', 'State', ['country_id' => $country->id]);
        $firstCountyOfSecondState = $this->create('counties', 'County', ['state_id' => $fourthStateOfCountry->id, 'tax_rate' => 5]); // 9
        $secondCountyOfSecondState = $this->create('counties', 'County', ['state_id' => $fourthStateOfCountry->id, 'tax_rate' => 4]);

        $fifthStateOfCountry = $this->create('states', 'State', ['country_id' => $country->id]);
        $firstCountyOfSecondState = $this->create('counties', 'County', ['state_id' => $fifthStateOfCountry->id, 'tax_rate' => 5]); // 11
        $secondCountyOfSecondState = $this->create('counties', 'County', ['state_id' => $fifthStateOfCountry->id, 'tax_rate' => 6]);

        $countyNotOfAnyState = $this->create('counties', 'County', ['tax_rate' => 3]);

        $expectedAverageRate = 5; // 55%11

        $this->assertEquals($expectedAverageRate, $country->averageRate());
    }
    
    /** @test */
	public function output_the_overall_taxes_of_the_country() {
        $country = $this->create('countries', 'Country');

        $firstStateOfCountry = $this->create('states', 'State', ['country_id' => $country->id]);
        $firstCountyOfFirstState = $this->create('counties', 'County', ['state_id' => $firstStateOfCountry->id, 'tax_amount' => 1000]);
        $secondCountyOfFirstState = $this->create('counties', 'County', ['state_id' => $firstStateOfCountry->id, 'tax_amount' => 4000]); // 8000
        $thirdCountyOfFirstState = $this->create('counties', 'County', ['state_id' => $firstStateOfCountry->id, 'tax_amount' => 3000]);

        $secondStateOfCountry = $this->create('states', 'State', ['country_id' => $country->id]);
        $firstCountyOfSecondState = $this->create('counties', 'County', ['state_id' => $secondStateOfCountry->id, 'tax_amount' => 2000]); // 10000
        $secondCountyOfSecondState = $this->create('counties', 'County', ['state_id' => $secondStateOfCountry->id, 'tax_amount' => 8000]);

        $thirdStateOfCountry = $this->create('states', 'State', ['country_id' => $country->id]);
        $firstCountyOfSecondState = $this->create('counties', 'County', ['state_id' => $thirdStateOfCountry->id, 'tax_amount' => 1000]); // 3000
        $secondCountyOfSecondState = $this->create('counties', 'County', ['state_id' => $thirdStateOfCountry->id, 'tax_amount' => 2000]);

        $fourthStateOfCountry = $this->create('states', 'State', ['country_id' => $country->id]);
        $firstCountyOfSecondState = $this->create('counties', 'County', ['state_id' => $fourthStateOfCountry->id, 'tax_amount' => 1000]); // 4000
        $secondCountyOfSecondState = $this->create('counties', 'County', ['state_id' => $fourthStateOfCountry->id, 'tax_amount' => 3000]);

        $fifthStateOfCountry = $this->create('states', 'State', ['country_id' => $country->id]);
        $firstCountyOfSecondState = $this->create('counties', 'County', ['state_id' => $fifthStateOfCountry->id, 'tax_amount' => 1000]); // 5000
        $secondCountyOfSecondState = $this->create('counties', 'County', ['state_id' => $fifthStateOfCountry->id, 'tax_amount' => 4000]);

        $countyNotOfAnyState = $this->create('counties', 'County', ['tax_amount' => 3000]);

        $expectedOverallAmount = 30000; // 8+10+3+4+5 = 30

        $this->assertEquals($expectedOverallAmount, $country->overallAmount());
    }
}